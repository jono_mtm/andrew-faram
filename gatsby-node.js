require("ts-node").register({ files: true });

const path = require("path");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const results = await graphql(`
    {
      allContentfulContentPage {
        edges {
          node {
            id
            title
          }
        }
      }
      allContentfulGalleryPage {
        edges {
          node {
            id
            title
          }
        }
      }
    }
  `);

  if (results.errors) throw results.errors;

  const ContentPage = path.resolve("./src/templates/contentPage.tsx");
  const GalleryPage = path.resolve("./src/templates/galleryPage.tsx");

  results.data.allContentfulContentPage.edges.forEach((page) => {
    createPage({
      path: `/${page.node.title}`,
      component: ContentPage,
      context: {
        id: page.node.id,
      },
    });
  });

  results.data.allContentfulGalleryPage.edges.forEach((page) => {
    createPage({
      path: `/${page.node.title}`,
      component: GalleryPage,
      context: {
        id: page.node.id,
      },
    });
  });
};
