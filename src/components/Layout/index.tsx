import React from "react";
import "antd/dist/antd.css";
import "./global.css";
import styled from "@emotion/styled";
import Footer from "./Footer";
import Header from "./Header";

const Content = styled.div``;

const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <Content>{children}</Content>
      <Footer />
    </>
  );
};

export default Layout;
