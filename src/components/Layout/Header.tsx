import styled from "@emotion/styled";
import { Affix, Typography, PageHeader, Row } from "antd";
import Avatar from "antd/lib/avatar/avatar";
import { Link } from "gatsby";
import React from "react";
import { useResponsive } from "../../utils/responsive";
import { useRoutes } from "../../utils/routes";
import { PageType } from "../../utils/types";
import Logo from "../ui/Logo";
import Socials from "../ui/Socials";

const { Text } = Typography;

const StyledHeader = styled(PageHeader)`
  background: #111;
`;

const ItemWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 15px;
`;

const StyledNav = styled(Row)`
  background-color: white;
`;

const Indicator = styled.div<{ active?: boolean }>`
  padding: 4px;
  border-radius: 200px;
  border: 2px solid;
  border-color: ${(props) => (props.active ? "#111" : "#ddd")};
  margin-bottom: 5px;
  transition: border-color 0.1s ease;
  &:hover {
    border-color: #777;
  }
`;

const Route: React.FC<{ route: PageType }> = ({ route }) => {
  const { title, thumbnail } = route;
  const url = typeof window !== "undefined" ? window.location.href : "";
  const active = url.indexOf(title) > -1;
  return (
    <Link to={`/${title}`}>
      <ItemWrapper>
        <Indicator active={active}>
          <Avatar size={50} src={thumbnail?.fixed?.src || ""} />
        </Indicator>
        <Text>{title}</Text>
      </ItemWrapper>
    </Link>
  );
};

const Nav: React.FC = () => {
  const routes = useRoutes();
  const { isMobile } = useResponsive();
  // const url = typeof window !== "undefined" ? window.location.href : "";
  // const orderedRoutes = [
  //   ...routes.filter((route) => url.indexOf(route.title) > -1),
  //   ...routes.filter((route) => url.indexOf(route.title) < 0),
  // ];

  if (isMobile)
    return (
      <StyledNav justify="center">
        <div style={{ overflowY: "auto", display: "flex" }}>
          {routes.map((route) => {
            return <Route key={route.id} route={route} />;
          })}
        </div>
      </StyledNav>
    );
  return (
    <StyledNav justify="center">
      <div style={{ overflowY: "auto", display: "flex" }}>
        {routes.map((route) => {
          return <Route key={route.id} route={route} />;
        })}
      </div>
    </StyledNav>
  );
};

const Header: React.FC = () => {
  const { isMobile } = useResponsive();
  if (isMobile)
    return (
      <>
        <div
          style={{
            background: "#111",
            display: "flex",
            justifyContent: "space-between",
            alignContent: "center",
            padding: "1rem",
          }}
        >
          <Link to="/">
            <Logo />
          </Link>
          <Socials />
        </div>
        <Affix>
          <Nav />
        </Affix>
      </>
    );
  return (
    <Affix>
      <StyledHeader
        title={
          <Link to="/">
            <Logo />
          </Link>
        }
        extra={<Socials />}
      />
      <Nav />
    </Affix>
  );
};

export default Header;
