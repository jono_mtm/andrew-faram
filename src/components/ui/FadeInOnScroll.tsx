import React, { ReactNode, useState, useEffect } from "react";
import { useInView } from "react-intersection-observer";

interface Props {
  children: string | ReactNode;
}

const FadeInOnScroll: React.FC<Props> = ({ children }) => {
  const [ref, inView] = useInView();

  const [isVisible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    if (inView) setVisible(true);
  }, [inView]);

  return (
    <div ref={ref}>
      {isVisible ? (
        <div className="fade-in-from-top">{children}</div>
      ) : (
        <div style={{ opacity: 0 }}>{children}</div>
      )}
    </div>
  );
};

export default FadeInOnScroll;
