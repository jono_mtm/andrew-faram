import styled from "@emotion/styled";
import React from "react";

const StyledHeading = styled.h1`
  text-align: center;
  margin: 2rem 0 2rem 0;
`;

const PageTitle: React.FC = ({ children }) => {
  return <StyledHeading>{children}</StyledHeading>;
};
export default PageTitle;
