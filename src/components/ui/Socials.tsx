import React from "react";

import Row from "antd/lib/row";
import Avatar from "antd/lib/avatar/avatar";
import { socials } from "../../utils/config";

const Socials: React.FC = () => {
  return (
    <Row justify="center">
      {socials.map((social) => {
        const { name, url, icon } = social;
        return (
          <a target="_blank" rel="noreferrer noopener" href={url} key={name}>
            <Avatar
              size="large"
              style={{ background: "transparent" }}
              icon={icon}
            />
          </a>
        );
      })}
    </Row>
  );
};

export default Socials;
