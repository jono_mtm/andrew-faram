import React, { Component, useEffect, useState } from "react";
import PaymentForm from "./PaymentForm";

const Checkout = () => {
  const [isLoaded, setLoaded] = useState<boolean>(false);

  const sqPaymentScript = document.createElement("script");
  sqPaymentScript.src = "https://js.squareup.com/v2/paymentform";
  sqPaymentScript.type = "text/javascript";
  sqPaymentScript.async = false;

  document.getElementsByTagName("head")[0].appendChild(sqPaymentScript);

  useEffect(() => {
    sqPaymentScript && setLoaded(true);
  }, [sqPaymentScript]);

  if (!isLoaded) return null;

  const windowAny = window as any;

  return <PaymentForm paymentForm={windowAny.SqPaymentForm} />;
};

export default Checkout;
