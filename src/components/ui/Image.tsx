import styled from "@emotion/styled";
import React, { CSSProperties, useState } from "react";

const Img = styled.div<{ src: string; blurred?: boolean }>`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;

  filter: ${(props) => (props.blurred ? `blur(1px)` : undefined)};
  overflow: hidden;
  background-image: ${(props) => props.src};
  background-position: center;
  background-size: cover;
`;

interface Props {
  src: string;
  placeholder: string;
  style?: CSSProperties;
}

const Image: React.FC<Props> = ({
  src,
  placeholder,
  style = { width: "100%", height: "100%", display: "block" },
}) => {
  const [isImageLoading, setImageLoading] = useState<boolean>(true);

  const wrapperStyle: CSSProperties = {
    position: "relative",
    overflow: "hidden",
    ...style,
  };

  return (
    <div style={{ ...wrapperStyle }}>
      <img
        src={src}
        style={{ display: "none" }}
        alt={src || ""}
        onLoad={() => setImageLoading(false)}
      />
      <Img blurred className="fade-in" src={`url('${placeholder}')`} />
      {!isImageLoading && <Img className="fade-in" src={`url('${src}')`} />}
    </div>
  );
};

export default Image;
