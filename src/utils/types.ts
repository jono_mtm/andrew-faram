export type ImageType = {
  fixed: { src: string };
};

export type GalleryImageType = {
  id: string;
  title: string;
  description: string;
  fixed: { src: string };
  fluid: { src: string };
};

export type ContentType = {
  json: any;
};

export type DescriptionType = {
  description: string;
};

export type PackageType = {
  id: string;
  image?: ImageType;
  title: string;
  description?: DescriptionType;
  details?: string;
  price: string;
};

export type ProductType = {
  id: string;
  title: string;
  price: number;
  checkoutLink: string;
  description?: DescriptionType;
  images: ImageType[];
};

export type VideoType = {
  id: string;
  title: string;
  description?: ContentType;
  link?: string;
};

export type ClientType = {
  id: string;
  name?: string;
  logo?: ImageType;
  link?: string;
};

export type PressArticleType = {
  id: string;
  title?: string;
  link?: string;
  content?: ContentType;
};

export type BookingFormSectionType = {
  id: string;
  internal: { type: "ContentfulBookingFormSection" };
  title: string;
  description?: ContentType;
};

export type ClientsSectionType = {
  id: string;
  internal: { type: "ContentfulClientsSection" };
  title?: string;
  description?: ContentType;
  clients: ClientType[];
};

export type MediaKitSectionType = {
  id: string;
  internal: { type: "ContentfulMediaKitSection" };
  description?: ContentType;
};

export type PackagesSectionType = {
  id: string;
  internal: { type: "ContentfulPackagesSection" };
  packages: PackageType[];
};

export type PressReleaseSectionType = {
  id: string;
  internal: {
    type: "ContentfulPressReleaseSection";
  };
  articles?: PressArticleType[];
};

export type StoreSectionType = {
  id: string;
  internal: { type: "ContentfulStoreSection" };
  products: ProductType[];
};

export type VideographySectionType = {
  id: string;
  internal: { type: "ContentfulVideographySection" };
  videos: VideoType[];
};

export type SectionType =
  | BookingFormSectionType
  | ClientsSectionType
  | MediaKitSectionType
  | PackagesSectionType
  | PressReleaseSectionType
  | StoreSectionType
  | VideographySectionType;

export type PageType = {
  id: string;
  title: string;
  content?: ContentType;
  thumbnail: ImageType;
  sections?: SectionType[];
  images?: GalleryImageType[];
  order: number;
};
