import { useMediaQuery } from "react-responsive";
import { useState, useEffect } from "react";

const breakpoints = {
  mobile: `1000px`,
};

export const useResponsive = () => {
  const isMobile = useMediaQuery({
    query: `(max-width: ${breakpoints.mobile})`,
  });

  const [isMobileHook, setIsMobileHook] = useState<boolean>(true);

  useEffect(() => {
    setIsMobileHook(isMobile);
  }, [isMobile]);

  return { isMobile: isMobileHook };
};
