import React from "react";

import {
  FacebookFilled,
  InstagramFilled,
  YoutubeFilled,
} from "@ant-design/icons";

import { createFromIconfontCN } from "@ant-design/icons";

const IconFont = createFromIconfontCN({
  scriptUrl: "//at.alicdn.com/t/font_2262115_169vlnosszf.js",
});

const iconStyle = { height: 20, width: 20, fontSize: 20 };

type SocialType = {
  name: string;
  url: string;
  icon: React.ReactNode;
};

export const socials: SocialType[] = [
  {
    name: "facebook",
    url: "https://www.facebook.com/andrewfaramphotography",
    icon: <FacebookFilled style={{ ...iconStyle }} />,
  },
  {
    name: "vimeo",
    url: "https://www.vimeo.com/267002314",
    icon: <IconFont type="icon-vimeo" style={{ ...iconStyle }} />,
  },
  {
    name: "instagram",
    url: "https://www.instagram.com/andrewfaramphotography",
    icon: <InstagramFilled style={{ ...iconStyle }} />,
  },
  {
    name: "youtube",
    url: "https://www.youtube.com/channel/UCtBfjTP4oGXrMiVBCJn6v6w",
    icon: <YoutubeFilled style={{ ...iconStyle }} />,
  },
];
